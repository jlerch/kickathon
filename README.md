# Kickathon!
>This project is meant to be used to teach other people how to set up a python project. The original use case was the introduction of team members to software development prior to a hackathon.

Hey fellow people *wave*. I just created this awesome project skeleton so that we can start off our project as fast as possible... Wait... I just realize... Somebody was hungry and just ate crucial parts of the code! Can you help me building it up again? A nice side effect is that you get used to the developement environment and can get going fast once the hackathon starts.

Down below the tasks you can find a quick introduction to git.

## Python Crash Course
### Let's get going!
1. This might be the first challenge. Figure out, how to use the version management tool "git" to clone this project to your personal computer. 
2. Set up a virtual environment and activate it. Install all your python packages belonging to the project in there. Make sure, the environment is activated every time, you work within this project. You can use the following commands (if you are using Linux):
```
$ python3 -m venv venv # create a virtual environment
$ source venv/bin/activate
```
3. Install the project by typing `python3 install -e .` while being in project root (if this is not possible, fix the errors).
4. Execute the just installed python package `python3 -m kick --help` and try out executing the functions.

If you went through all the steps above: Great job! Now you have set up a nice working environment for python development. If you are still motivated you can go through the following sections.

### Follow-up tasks 
By completing these tasks, you will learn some very useful skills to use python for basic data analysis. You will also learn how to use python libraries in general. If you have a question because you do not know how to proceed, feel free to ask! :) You will work with the dataset `oktoberfest.csv` containing data about how much chicken (in pieces) and beer (in liters) was bought during the oktoberfest over the years.
- Write a new function in `__main__.py` reading the file `oktoberfest.csv` and calculate the mean of visitors over the years (make sure the newly defined function is still above the statement `if __name__ ...` in the second last line).
    - A useful package here is `pandas`. Search it's documentation on the web.  
    - Especially the functions `read_csv()` and `mean()` are useful.  
    - It might also be useful to read a little bit on what a data frame is in pandas.   
    - If you already know some data framework (e.g. excel, R, etc.), [this source](https://pandas.pydata.org/docs/getting_started/index.html) explains, how to get from there to pandas. However, feel free to read other documentation as well, if you want to do so :).
- Write another function, that again reads the file `oktoberfest.csv` and plots a line plot of the amount of visitors over the years.
    - A useful package for doing this is `matplotlib.pyplot` with its function `plot()`.

Did you complete these tasks? Congratulations! You have learned quite some nice skills to write python code. If the above tasks took you some time, that is absolutely no problem. It means, you have learnt something :). If you are eager to proceed you can write me and I can give you some more of these kind of tasks.

If you think "oh maaaan, I am very bored the tasks above are too easyyy. I do not even want other easy tasks, I want to do the real stuff", say no more! Below, I listed some advanced tasks, you can follow ;).

### Advanced tasks 
- How to build a CLI with help command (`python -m kick --help`) (In fact there is already a CLI set up, you just have to find out how to extend it).
    - The package you want to use for that is `typer`.
    - Create at least one new command to the CLI.
    - Find out how to add descriptions in the help page for that command and do it.
    - Add some arguments to your command and add some help page descriptions to them, too.
- Build a simple linear regression model using `sklearn` estimating the beer price based on the year.
- Research how to create a class with abstract methods.
- If you want another task, feel free to ask me, I have some other ideas too.

### Pro Tasks
These tasks, I myself would have to research on how to achieve them. Do not feel bad if you do not know what they mean. However, if you are _still_ motivated, you can start working on those :)
- Create a CLI with state

## Quick Introduction to Git
This is not important for you solving the kickathon above. However, it will be a crucial part of our work during the hackathon. If you have no motivation to read that part yet, this is fine. We can also have an introduction to git at the hackathon.

First, read about what git is and how it works. You can do so at [this website](https://www.freecodecamp.org/news/what-is-git-and-how-to-use-it-c341b049ae61/).

Take your time, I will wait here :) ...

...

...

Read it? Nice! With all that new knowledge, we can talk about how to use git during the hackathon. There are multiple ways to do that. The way I propose to use is called _feature branch_ and it works as follows: Say, the project has a working software in the branch `main` and you would like to add something new to it, for example a nice GUI. We call this new thing a _feature_. Before changing the code to include the feature we do the following:
```
# Create a new branch and switch to it.
git checkout -b add-gui
# Change the code. Every time you make a significant change during the GUI developement process, please create a commit for that. For example, if you added a new function that creates a window, you...
# ...add the changed files to the staging area
git add changed_file1.py changed_file2.py
# ...and commit them. Remember to assign commit message as descriptive as possible! This is important to later see what was done when.
git commit -m "add function to create a window"
# After that, you should push (upload) your changes to the remote repository (e.g. on GitLab). This makes them accessible by others and the work easier.
git push origin
# Once you are finished with implementing your feature and you tested that everything works, you can create a merge request at the website of the repository.
```


