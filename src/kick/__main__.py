from typer import Typer

app = Typer()

def hello():
    print("Yay, it works!")

def whatsyourname():
    print("Hi, my name is kick :) I am a freshly born python package!")

if __name__ == "__main__":
    app()
